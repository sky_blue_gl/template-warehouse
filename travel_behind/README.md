## SpringBoot+MyBatisPlus+VUE+Axios+ElmentUI+knife4j项目开发模板

### 项目视频介绍：[https://www.bilibili.com/video/BV1Yk4y1K7VH/](https://www.bilibili.com/video/BV1Yk4y1K7VH/)

### 核心部分：

SpringBoot：基础框架

MyBatisPlus：数据交互框架

VUE、AXIOS、Element：前端框架

### 组件部分：

knife4j：接口测试工具

mybatis-plus-generator：mybatisplus代码生成器

hutool：工具包

lombok：省去get、set方法

### 基础功能

过滤器：用户未登录的情况下禁止请求内部资源

统一返回接口：更利于前后端交互

统一异常处理：进行统一的异常处理

mybatisplus分页插件：用于分页控制

登录：拥有基本的登录功能


访问地址:http://localhost:8007/travelpage/pages/user/login.html

Knife4j访问地址：http://localhost:8007/doc.html#/home
