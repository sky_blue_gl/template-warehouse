package com.fang.travel.config;


import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.oas.annotations.EnableOpenApi;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;

@Configuration
@EnableOpenApi
class SwaggerConfig {
    /*创建API 应用
     * apiInfo() 增加API 相关信息
     * 通过select()函数返回一个ApiSelectorBuilder实例，用来控制哪些接口暴露Swagger 来 展示
     * */

    /*自己定义名字*/
    @Bean(value = "xxxAPI")
    public Docket xxxAPI() {
        Docket docket=new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(apiInfo())
                .host("http://localhost:ip/port")
                .select()
                /*扫描路径 我的是controller层*/
                .apis(RequestHandlerSelectors.basePackage("com.fang.travel.controller"))
                .paths(PathSelectors.any())
                .build();
        return docket;
    }

    private ApiInfo apiInfo() {
        return new ApiInfoBuilder().
                title("XXX接口文档")
                .description("仅用于项目开发/测试/对接使用")
                .termsOfServiceUrl("")
                //编写你的联系方式 name url email
                .contact(new Contact("XXX", "http://www.baidu.com", "12313@163.com"))
                .version("1.1")
                .build();
    }
}
