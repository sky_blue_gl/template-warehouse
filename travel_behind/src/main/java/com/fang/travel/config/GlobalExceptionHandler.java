package com.fang.travel.config;


import com.fang.travel.common.Result;
import com.fang.travel.exceptionhandler.UserException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * 全局异常处理
 * ExceptionHandler注解就是一个典型的例子，它会在运行时使用反射技术来找到需要处理的异常类型，并将处理结果封装成JSON格式返回给客户端
 * 反射技术可以让程序在运行时动态地获取类的信息，而不需要在编译时就确定类的类型。通过反射，可以在运行时动态地创建对象、调用方法、访问属性等，从而增强了程序的灵活性和扩展性。
 * @author 90632
 */
@ControllerAdvice
@Slf4j
public class GlobalExceptionHandler {
    /**
     * 全局异常
     * @param e
     *
     */
    @ExceptionHandler(Exception.class)
    @ResponseBody
    public Result<?> exceptionHandler(Exception e){
        e.printStackTrace();
        return Result.fail(505,"抱歉出现了一点问题！");
    }

    @ExceptionHandler(UserException.class)
    @ResponseBody
    public Result<?> userExceptionHandler(UserException e){
        e.printStackTrace();
        return Result.fail(502,"用户中心出现问题！");
    }
}
