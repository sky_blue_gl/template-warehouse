package com.fang.travel.mapper;

import com.fang.travel.entity.User;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author test
 * @since 2023-06-25
 */
@Mapper
public interface UserMapper extends BaseMapper<User> {

}
