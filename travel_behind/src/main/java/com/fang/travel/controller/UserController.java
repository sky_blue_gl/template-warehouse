package com.fang.travel.controller;

import com.fang.travel.common.Result;
import com.fang.travel.entity.User;
import com.fang.travel.exceptionhandler.UserException;
import com.fang.travel.service.IUserService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import javax.servlet.http.HttpSession;

/**
 * <p>
 *  前端控制器
 * </p>
 * @author test
 * @since 2023-06-25
 */

@RestController
@RequestMapping("/user")
public class UserController{

    @Autowired
    private IUserService userService;

    @ApiOperation("登录接口")
    @PostMapping("/login")
    public Result<?> userLogin(@RequestBody User user, HttpSession session) throws UserException{

//                try {
//                    int a=1/0;
//                }catch (Exception e){
//                    throw  new UserException();
//                }

                if(userService.login(user,session)) {
                    return  Result.success("登录成功");
                } else {
                    return  Result.fail("用户名或密码错误");
                }
    }
}
