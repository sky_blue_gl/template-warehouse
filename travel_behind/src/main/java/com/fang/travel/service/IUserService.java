package com.fang.travel.service;

import com.fang.travel.entity.User;
import com.baomidou.mybatisplus.extension.service.IService;

import javax.servlet.http.HttpSession;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author test
 * @since 2023-06-25
 */
public interface IUserService extends IService<User> {

    Boolean login(User user, HttpSession session);
}
