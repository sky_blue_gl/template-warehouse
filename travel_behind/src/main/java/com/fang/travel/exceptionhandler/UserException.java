package com.fang.travel.exceptionhandler;

/**
 * @author 90632
 */
public class UserException extends RuntimeException {
    private static final long serialVersionUID = 1364225358754654702L;

    public UserException(){
        super("用户异常");
    }

    public UserException(String message){
        super(message);
    }
}
